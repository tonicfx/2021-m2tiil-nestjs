import { BadRequestException, Injectable } from '@nestjs/common';
import { User } from '../entity/user.entity';
import { IUserService } from './iuser.service';

@Injectable()
export class UserMockService implements IUserService{
    
    private users : Array<User> = [{
        id:1,
        fullName: "Anthony M",
        birthday:"12/02/2000",
        isActive:true,
        password:"toto"
    },
    {
        id:2,
        fullName: "Cecile M",
        birthday:"14/12/2002",
        isActive:true,
        password:"tata"
    }]


    findAll(): Promise<User[]> {
        return Promise.resolve(this.users);
    }

    getUser(id: number): Promise<User> {
        let user = this.users.find(user=>{
            if(user.id == id){
                return user
            }
        })

       return Promise.resolve(user)
    }

    createUser(user: User) {
        this.users.push(user)
    }

    updateUser(user: User) {
        let userFind = this.users.find(userToTest=>{
            if(userToTest.id == user.id){
                return userToTest
            }
        })

        if(userFind){
            userFind.birthday=user.birthday
            userFind.fullName=user.fullName
            userFind.isActive=user.isActive
            userFind.password=user.password
        }
    }

    deleteUser(id: number) {
        let userFind = this.users.find(userToTest=>{
            if(userToTest.id == id){
                return userToTest
            }
        })
        if(userFind){
            this.users.splice(this.users.indexOf(userFind),1)
        }
    }

    async exist(username: string, password: string): Promise<Number> {
        return new Promise((resolve, reject) => {
            let element = this.users.find(element => element.fullName == username && element.password == password)
            resolve(element != undefined ? element.id : NaN)
        })
    }
}
