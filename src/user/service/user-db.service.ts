import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { User } from '../entity/user.entity';
import { IUserService } from './iuser.service';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class UserDbService implements IUserService{

    constructor(@InjectRepository(User) private usersRepository: Repository<User>) { }

    async findAll(): Promise<User[]> {
        console.info("get all users")
        return await this.usersRepository.find();
    }

    async getUser(id: number): Promise<User> {
        let users = await this.usersRepository.find({
            select: ["fullName", "birthday", "isActive"],
            where: [{ "id": id }]
        });
        return users.length > 0 ? users[0] : null
    }

    async createUser(user: User) {
        console.info("create user ",user);
        return await this.usersRepository.save(user)
    }

    async updateUser(user: User) {
        this.usersRepository.save(user)
    }

    async deleteUser(id:number) {
        let user = await this.usersRepository.find({
            select: ["fullName", "birthday", "isActive"],
            where: [{ "id": id }]
        })[0];
        this.usersRepository.delete(user);
    }

    async exist(username: string, password: string): Promise<Number> {
        return await this.usersRepository.find({
            select: ["id","fullName", "password", "birthday", "isActive"],
            where: [{ "fullName": username, "password": password }]
        }).then(result => {
            return result.length > 0 ? result[0].id : 0 
        }).catch(error => {
            return 0;
        })
    }
}
