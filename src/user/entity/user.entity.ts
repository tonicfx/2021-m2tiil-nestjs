import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    fullName:String;

    @Column()
    password:String;

    @Column()
    birthday:String;

    @Column({ default: true })
    isActive:boolean;
}