import { Module } from '@nestjs/common';
import { UserController } from './user/user.controller';
import { UserDbService } from './service/user-db.service';
import { User } from './entity/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserMockService } from './service/user-mock.service';

@Module({
  imports: [TypeOrmModule.forFeature([User])],
  controllers: [UserController],
  providers: [
    {
      provide: 'UserService',
      useClass: UserDbService
    }
  ]
})
export class UserModule {}
