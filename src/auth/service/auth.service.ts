import { Inject, Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { IUserService } from 'src/user/service/iuser.service';

@Injectable()
export class AuthService {

  constructor(@Inject('UserService') private readonly userService: IUserService, private jwtService: JwtService) {}

  async validateUser(username: string, password: string): Promise<any> {
    const id = await this.userService.exist(username, password);
    console.info(">>>> id : ",id)
    if(id != undefined && id != 0){
      return this.login(username, password)
    }else {
      return null
    }
  }

  async login(username: string, password: string) {
    const payload = { username: username, sub: password };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
