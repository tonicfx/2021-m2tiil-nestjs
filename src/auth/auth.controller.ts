import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from './jwt-auth.guard';
import { LocalAuthGuard } from './local-auth.guard';
import { AuthService } from './service/auth.service';


@Controller('auth')
export class AuthController {
    
    constructor(private readonly authService: AuthService) { }

    @UseGuards(LocalAuthGuard)
    @Post('login')
    async login(@Body() user: any) {
        return this.authService.login(user.fullName, user.password);
    }

    @UseGuards(LocalAuthGuard)
    @Post('test')
    async test(@Body() user: any) {
        return "ok"
    }
}
