import { Controller, Post, Body, Get, Put, Delete,Param, Inject, UseGuards} from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@Controller('test')
export class TestController {

    @UseGuards(JwtAuthGuard)
    @Get('/accesswithguard')
    get() {
        return "accesswithguard"
    }

    @Get('/accesswithoutguard')
    async findAll() {
        return "accesswithoutguard"
    }
}